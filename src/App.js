import "./App.css";
import {
  Typography,
  AppBar,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  CssBaseline,
  Grid,
  Toolbar,
  Container,
  Button,
} from "@material-ui/core";
import { PhotoCamera, Save } from "@material-ui/icons";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import useStyles from "./styles";
import { green, grey, orange, pink, red } from "@material-ui/core/colors";

const theme = createMuiTheme({
  typography: {
    h2: {
      fontSize: 36,
      marginBottom: 15,
      color: '#fabada'
    },
    fontFamily: 'Viaoda Libre',
  },
  palette: {
    primary: {
      main: orange[100],
    },
    secondary: {
      main: pink[100],
    },
    error: {
      main: red[900],
    },
    warning: {
      main: orange[500]
    },
    info: {
      main: grey[500],
    },
    success: {
      main: green[900],
    }
  },
});

const App = () => {
  const classes = useStyles();
  const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <PhotoCamera className={classes.icon} />
          <Typography variant="h6">Photo Album</Typography>
        </Toolbar>
      </AppBar>
      <main>
        <div className={classes.container}>
          <Container maxWidth="sm">
            <Typography
              variant="h1"
              align="center"
              color="secondary"
              gutterBottom
            >
              Album layout
            </Typography>
            <Typography
              variant="h6"
              align="center"
              color="textSecondary"
              paragraph
            >
              Hello everyone, this is a photo album blablabla bla bla bla bla
              rd6cujidfngudf ndfmghasudf gasdufg bausfg hufgh usahg
              ausghafgasgher r et e er uthuerht eru thr
            </Typography>
            <div className={classes.buttons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button variant="contained" color="primary">
                    See my photos!
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant="outlined"
                    color="secondary"
                    startIcon={<Save />}
                  >
                    Secondary action
                  </Button>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>

        <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
            {cards.map((card) => (
              // el 12 es TODA la pantalla, el 6 es la mitad así que tendremos 2 por línea
              // y el 4 es para tener 3 por línea (12/4=3)
              <Grid item xs={12} sm={6} md={4} key={card}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image="https://source.unsplash.com/random"
                    title="title"
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5">
                      Heading
                    </Typography>
                    <Typography>
                      This is a media card. You can use this section to discribe
                      the content.
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small" color="primary">
                      View
                    </Button>
                    <Button size="small" color="secondary">
                      Edit
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
      <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          Footer
        </Typography>
        <Typography variant="subtitle1" align="center" color="text">
          Something here!
        </Typography>
      </footer>
    </ThemeProvider>
  );
};

export default App;
