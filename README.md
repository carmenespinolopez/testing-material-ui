# Material ui application

Application build with React to test Material UI.

## Installation
- Clone the repository
```sh
git clone <link>
```
- Go to the location of the cloned repository
```sh
cd testing-material-ui
```
- Install the dependencies and start the application
```sh
npm i
npm start
```
- The application will automatically open at `http://localhost:3000/`.
- ✨ Magic ✨

### More information:
[Official material UI page](https://material-ui.com/)



